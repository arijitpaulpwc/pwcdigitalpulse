package stepDefination;

import Manager.BaseClassManager;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import pages.SearchResultPage;
import pages.SubscribePage;

public class SearchResultPageSteps extends BaseClassManager{
    Logger log = LogManager.getLogger(SearchResultPage.class);
    SearchResultPage searchResultPage;
    public SearchResultPageSteps() {
        this.searchResultPage = new BaseClassManager().searchResultPage();
    }
    @Then("^I am taken to the search results page$")
    public void NavigateToTheSearchResultPage(){
        searchResultPage.verifySearchResultPageIsDisplayed();
        log.info("Search Page is displayed successfully");
    }
    @And("^I am presented with at least \"(.*)\" search result$")
    public void presentWithSearchResult(String minNoOfSearchResult){
        searchResultPage.verifyNoOfSearchResult(minNoOfSearchResult);
        log.info("Verify the number of search result are displayed which is at least : "+minNoOfSearchResult);
    }


}
