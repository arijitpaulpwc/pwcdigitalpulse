package stepDefination;

import Manager.BaseClassManager;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import pages.HomePage;

public class HomePageSteps extends BaseClassManager{
    HomePage homePage;
    Logger log = LogManager.getLogger(HomePageSteps.class);
    public HomePageSteps() {
        this.homePage = new BaseClassManager().homePage();
    }
    @Given("^I navigate to the PwC Digital Pulse website$")
    public void navigateToThePwCDigitalPulseWebsite(){
        homePage.navigateToHomePage();
        log.info("Navigated to the Home Page");
    }
    @When("^I am viewing the \"(.*)\" page$")
    public void verifyUserIsAtHomePage(String PageName){
        homePage.verifyHomePageIsDisplayed(PageName);
        log.info("Verify User is at the Home Page");
    }
    @Then("^I am presented with \"(.*)\" columns of articles$")
    public void verifyColumnAtHomePage(String columnNumber){
        int colNum = Integer.parseInt(columnNumber);
        homePage.verifyTotalNumberOfColumn(colNum);
        log.info("Verified total number of columns are present at the home page.");
    }
    @And("^The ‘left’ column is displaying \"(.*)\" articles$")
    public void verifyLeftColumnArticle(String LeftColumnArticle){
        int leftColumnArticleNum = Integer.parseInt(LeftColumnArticle);
        homePage.verifyTotalArticleOfLeftColumn(leftColumnArticleNum);
        log.info("Verified total number of Left columns articles are present");
    }
    @And("^The ‘middle’ column is displaying \"(.*)\" articles$")
    public void verifyMiddleColumnArticle(String MiddleColumnArticle){
        int middleColumnArticleNum = Integer.parseInt(MiddleColumnArticle);
        homePage.verifyTotalArticleOfMiddleColumn(middleColumnArticleNum);
        log.info("Verified total number of Middle columns articles are present");
    }
    @And("^The ‘right’ column is displaying \"(.*)\" articles$")
    public void verifyTotalArticleOfRightColumn(String RightColumnArticle){
        int rightColumnArticleNum = Integer.parseInt(RightColumnArticle);
        homePage.verifyTotalArticleOfRightColumn(rightColumnArticleNum);
        log.info("Verified total number of Right columns articles are present");
    }
    @When("^I click on the 'Subscribe' navigation link$")
    public void clickOnTheSubscribeNavigationLink(){
        homePage.clickOnSubscribeNavigationLink();
        log.info("Clicked on the Subscribe navigation Link");
    }
    @When("^I click on the ‘Magnifying glass’ icon to perform a search$")
    public void clickOnTheMagnifyingGlassIconToPerformSearch(){
        homePage.clickOnMagnifyingGlassIcon();
        log.info("Clicked on the Magnifying Glass Icon");
    }
    @And("^I enter the text \"(.*)\"$")
    public void IEnterTheText(String searchText){
        homePage.enterTextIntoSearchField(searchText);
        log.info("Entered the text into the search field. : "+searchText);
    }
    @And("^I submit the search$")
    public void iSubmitTheSearch(){
        homePage.clickOnSearchSubmit();
        log.info("Clicked on Submit search button");
    }
}
