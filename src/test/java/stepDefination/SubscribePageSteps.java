package stepDefination;

import Manager.BaseClassManager;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.DataTableType;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import pages.SubscribePage;

public class SubscribePageSteps extends BaseClassManager{
    SubscribePage subscribePage;
    Logger log = LogManager.getLogger(SubscribePageSteps.class);
    public SubscribePageSteps() {
        this.subscribePage = new BaseClassManager().subscribePage();
    }
    @Then("^I am taken to the 'Subscribe' page$")
    public void verifyUserIsAtHomePage(){
        subscribePage.verifySubscribePageIsDisplayed();
        log.info("Subscribe page is displayed successfully");
    }
    @And("^I am presented with the below fields$")
    public void IAmPresentedWithTheBelowFields(DataTable dataTable){
        subscribePage.verifyAllTheFields(dataTable);
        log.info("verified all the fields");
    }
    @And("^I will need to complete Google reCAPTCHA before I can complete my request$")
    public void completeGoogleReCAPTCHABeforeCompleteRequest(){
        subscribePage.validateReCaptchaIsDisplayed();
        log.info("validated the google reCAPTCHA is displayed successfully");
    }
}
