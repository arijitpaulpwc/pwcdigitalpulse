package stepDefination;

import Manager.BaseClassManager;
import Manager.DriverManager;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import java.io.File;

public class Hooks {
    DriverManager driverManager;
    WebDriver driver;
    //This will call before each scenario
    @Before
    public void beforeScenario(){
        BaseClassManager baseClassManager = new BaseClassManager();
        driverManager = baseClassManager.driverManager();
        driver = driverManager.getDriver();
    }
    //This will call after each scenario
    @After(order = 0)
    public void afterScenario(){
        driverManager.closeDriver();
    }
    @After(order=1)
    public void takeScreenshot(Scenario scenario){
        if(scenario.isFailed()){
            try {
                byte[] data = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
                scenario.attach(data, "image/png", "Failed Screenshot");
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }
}
