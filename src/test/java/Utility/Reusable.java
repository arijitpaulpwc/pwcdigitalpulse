package Utility;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.time.Duration;
import java.util.List;

public class Reusable {
    private WebDriver driver;
    public Reusable(WebDriver driver){
        this.driver = driver;
    }
    //Wait till the element is clickable
    public void waitForElement(By element,int duration){
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(duration));
        wait.until(ExpectedConditions.elementToBeClickable(element));
    }
    //wait until the element is visible
    public void untilTheElementVisible(By element,int duration){
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(duration));
        wait.until(ExpectedConditions.visibilityOfElementLocated(element));
    }
    //Return the element once the element is visible
    public WebElement untilVisible(By element, int duration){
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(duration));
        wait.until(ExpectedConditions.visibilityOfElementLocated(element));
        return driver.findElement(element);
    }
    //click on a WebElement
    public void clickOn(By element){
        waitForElement(element, 30);
        driver.findElement(element).click();
    }
    // verify the element is displayed
    public void verifyElementIsDisplayed(String xpathString){
        WebElement element = driver.findElement(By.xpath(xpathString));
        Assert.assertTrue(element.isDisplayed());
    }
    //this will perform scrolling the webpage
    public void scroll(int x, int y){
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("javascript:window.scrollBy("+x+","+y+")");
    }
}
