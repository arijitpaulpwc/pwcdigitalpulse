package runner;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

//This is a runner file, This will execute the feature file.
@RunWith(Cucumber.class)
@CucumberOptions(
        features = {
                "src/test/resources/features/PwcDigitalPulse.feature",
        },
        glue = {"stepDefination"},
        plugin= {
                "com.aventstack.extentreports.cucumber.adapter.ExtentCucumberAdapter:",
                "pretty","html:reports/cucumber-html-report.html",
                "json:reports/CucumberTestReport.json",
        },
        tags = ("@Sanity or @Regression")

)
public class CucumberRunner {

}

