package pages;

import Manager.BaseClassManager;
import Manager.DriverManager;
import Manager.ReaderManager;
import Utility.Reusable;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.List;

public class HomePage extends BaseClassManager{
    private WebDriver driver;
    private static final By pageTitle = By.xpath("(//p[contains(text(),'Digital Pulse')])[1]");
    private static final By totalColumns = By.xpath("//div[contains(@class,'headline_column')]");
    String totalArticles = "//div[contains(@class,'headline_column')][*newValue*]/article";
    private static final By subscribeButton = By.xpath("(//a[contains(@navigation-title,'Subscribe')])[1]");
    private static final By magnifyingSearchIcon = By.xpath("//button[text()=\"Search\"]");
    private static final By searchTextField = By.xpath("//input[@name='searchfield']");
    private static final By submitSearchButton = By.xpath("//input[@type='submit']");

    public HomePage(WebDriver driver){
        this.driver = driver;
    }

    //Navigate to the Home Page
    public void navigateToHomePage(){
        driverManager().launchApplication(new ReaderManager().getValue("URL"));
        System.out.println(super.driver);
        reusable().untilTheElementVisible(pageTitle,10);
    }
    //Verify the home page is displayed properly
    public void verifyHomePageIsDisplayed(String page){
        if(page.equalsIgnoreCase("home")){
            WebElement element = reusable().untilVisible(pageTitle,10);
            Assert.assertTrue(element.isDisplayed());
        }
    }
    //Verify the total number of columns are present at the Homepage
    public void verifyTotalNumberOfColumn(int colNum){
        List<WebElement> totalCols = driver.findElements(totalColumns);
        Assert.assertEquals(totalCols.size(),colNum);
    }
    //Verify the Total number of article present on left column
    public void verifyTotalArticleOfLeftColumn(int articleNum){
        String articles = totalArticles.replace("*newValue*","2");
        List<WebElement> totalArticles = driver.findElements(By.xpath(articles));
        Assert.assertEquals(totalArticles.size(),articleNum);
    }
    //Verify the Total number of article present on middle column
    public void verifyTotalArticleOfMiddleColumn(int articleNum){
        String articles = totalArticles.replace("*newValue*","1");
        List<WebElement> totalArticles = driver.findElements(By.xpath(articles));
        Assert.assertEquals(totalArticles.size(),articleNum);
    }
    //Verify the Total number of article present on right column
    public void verifyTotalArticleOfRightColumn(int articleNum){
        String articles = totalArticles.replace("*newValue*","3");
        List<WebElement> totalArticles = driver.findElements(By.xpath(articles));
        Assert.assertEquals(totalArticles.size(),articleNum);
    }
    //click on the subscribe Navigation link
    public void clickOnSubscribeNavigationLink(){
        reusable().clickOn(subscribeButton);
    }
    //click on the Magnifying Glass Icon at the Home Page
    public void clickOnMagnifyingGlassIcon() {
        reusable().clickOn(magnifyingSearchIcon);
    }
    //Enter text to the Search Field
    public void enterTextIntoSearchField(String searchText) {
        reusable().waitForElement(searchTextField,10);
        driver.findElement(searchTextField).sendKeys(searchText);
    }
    //CLick on the Search submit button
    public void clickOnSearchSubmit() {
        reusable().clickOn(submitSearchButton);
    }
}
