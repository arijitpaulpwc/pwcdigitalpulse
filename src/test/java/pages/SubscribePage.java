package pages;

import Manager.BaseClassManager;
import Manager.ReaderManager;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.DataTableType;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import stepDefination.HomePageSteps;

import java.util.List;
import java.util.Map;

public class SubscribePage extends BaseClassManager{
    private WebDriver driver;
    private static final By subscribeForm = By.xpath("//div[@id='onlineformstart']");
    String field = "//label[contains(text(),\"*newValue*\")]";
    String required = "//label[contains(text(),\"*newValue*\")]/span[@class=\"required\"]";
    String fieldType = "//label[contains(text(),\"*newValue*\")]/following-sibling::input";
    String fieldTypeDropdown = "//label[contains(text(),\"*newValue*\")]/following-sibling::select";
    private static final By reCAPTCHAIframe = By.xpath("//iframe[@title=\"reCAPTCHA\"]");
    private static final By reCAPTCHA = By.xpath("//div[@id=\"rc-anchor-container\"]");
    public SubscribePage(WebDriver driver){
        this.driver = driver;
    }
    Logger log = LogManager.getLogger(SubscribePage.class);
    //Verify Subscribe page is displayed properly
    public void verifySubscribePageIsDisplayed(){
        WebElement element = reusable().untilVisible(subscribeForm,10);
        Assert.assertTrue(element.isDisplayed());
    }
    //Verify all the fields
    public void verifyAllTheFields(DataTable dataTable){
        List<Map<String,String>> l = dataTable.asMaps();
        for(Map<String,String> table:l){
            try {
                String fieldName = field.replace("*newValue*", table.get("Field"));
                reusable().verifyElementIsDisplayed(fieldName);

                String requiredCheck = required.replace("*newValue*", table.get("Field"));
                if (table.get("Required").equalsIgnoreCase("true")) {
                    reusable().verifyElementIsDisplayed(requiredCheck);
                } else {

                }
                if (table.get("Type").equalsIgnoreCase("text") || table.get("Type").equalsIgnoreCase("email")) {
                    fieldType = fieldType.replace("*newValue*", table.get("Field"));
                    WebElement fieldTypeElement = driver.findElement(By.xpath(fieldType));
                    String actualType = fieldTypeElement.getAttribute("type");
                    Assert.assertEquals(actualType, table.get("Type"));
                } else if (table.get("Type").equalsIgnoreCase("dropdown")) {
                    System.out.println(table.get("Field"));
                    fieldTypeDropdown = fieldTypeDropdown.replace("*newValue*", table.get("Field"));
                    reusable().verifyElementIsDisplayed(fieldTypeDropdown);
                }
                log.info("Field-\""+table.get("Field")+"\" is Displayed - Pass");
            }catch (Exception e){
                log.error("Field-\""+table.get("Field")+"\" is not Displayed - \"Failed\"");
                Assert.fail();
            }
        }
    }
    //Verify the reCAPTCHA is displayed properly
    public void validateReCaptchaIsDisplayed(){
        driver.switchTo().frame(driver.findElement(reCAPTCHAIframe));
        WebElement element = reusable().untilVisible(reCAPTCHA,10);
        Assert.assertTrue(element.isDisplayed());
    }

}
