package pages;

import Manager.BaseClassManager;
import io.cucumber.datatable.DataTable;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.List;
import java.util.Map;

public class SearchResultPage extends BaseClassManager{
    private WebDriver driver;
    private static final By searchBox = By.xpath("//*[@class=\"sr-search\"]");
    private static final By searchResults = By.xpath("//div[@class=\"srp-list\"]/div");
    public SearchResultPage(WebDriver driver){
        this.driver = driver;
    }
    //Verify the search result page is displayed
    public void verifySearchResultPageIsDisplayed() {
        WebElement element = reusable().untilVisible(searchBox,10);
        Assert.assertTrue(element.isDisplayed());
    }
    //Verify the no of searchResult is at least 1 or more than 1
    public void verifyNoOfSearchResult(String minNoOfSearchResult) {
        reusable().waitForElement(searchResults, 20);
        List<WebElement> allSearchResults = driver.findElements(searchResults);
        Assert.assertTrue(allSearchResults.size()>=Integer.parseInt(minNoOfSearchResult));
    }
}
