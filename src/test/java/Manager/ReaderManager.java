package Manager;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class ReaderManager {
    private String propertyFilePath = "src/main/resources/config.properties";
    private Properties properties;
    //Read the Properties file and return that
    public Properties readFromProperties() throws IOException {
        FileReader reader = new FileReader(propertyFilePath);
        Properties props = new Properties();
        props.load(reader);
        return props;
    }
    //Base on the Key, It will return the value from the properties file
    public String getValue(String key) {
        try{
            return readFromProperties().getProperty(key);
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }
}
