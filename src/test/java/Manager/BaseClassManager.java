package Manager;

import Utility.Reusable;
import org.openqa.selenium.WebDriver;
import pages.HomePage;
import pages.SearchResultPage;
import pages.SubscribePage;

public class BaseClassManager{
    public static WebDriver driver;
    private HomePage homePage = null;
    private SubscribePage subscribePage = null;
    private SearchResultPage searchResultPage = null;
    private Reusable reusable = null;
    private static DriverManager driverManager = null;
    //return the HomePage class object
    public HomePage homePage(){
        homePage = (homePage==null)? new HomePage(driver) : homePage;
        return homePage;
    }
    //return the SubscribePage class object
    public SubscribePage subscribePage(){
        subscribePage = (subscribePage==null) ? new SubscribePage(driver) : subscribePage;
        return subscribePage;
    }
    //return the SearchResultPage class object
    public SearchResultPage searchResultPage(){
        searchResultPage = (searchResultPage==null) ? new SearchResultPage(driver) : searchResultPage;
        return searchResultPage;
    }
    //return the Reusable class object
    public Reusable reusable(){
        reusable = (reusable==null)? new Reusable(driver) : reusable;
        return reusable;
    }
    //return the DriverManager class object
    public static DriverManager driverManager(){
        if(driverManager==null){
            driverManager = new DriverManager();
            driver = DriverManager.getDriver();
            return driverManager;
        } else{
            driver = DriverManager.getDriver();
            return driverManager;
        }
    }

}
