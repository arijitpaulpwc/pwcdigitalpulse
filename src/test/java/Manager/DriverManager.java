package Manager;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

public class DriverManager {
    public static WebDriver driver;
    private static String browser = new ReaderManager().getValue("Browser");
    private static String driverPath = System.getProperty("user.dir") + new ReaderManager().getValue("DriverPath");
    private static String url = new ReaderManager().getValue("URL");

    //This will return the current webDriver
    public static WebDriver getDriver(){
        if(driver==null) {
            switch (browser) {
                case "CHROME":
                    System.setProperty("webdriver.chrome.driver", driverPath);
                    try{
                        driver = new ChromeDriver();
                    } catch (Exception e){
                        throw e;
                    }
                    break;
                case "FIREFOX" :
                    driver = new FirefoxDriver();
                    break;
                case "INTERNETEXPLORER" :
                    driver = new InternetExplorerDriver();
                    break;
                default:
                    System.out.println("Wrong user choice.");
                    break;
            }
        }
        driver.manage().window().maximize();
        return driver;
    }
    //This method will close the browser
    public void closeDriver(){
        driver.quit();
        driver = null;
    }
    //Launch the WebPage based on the provided URL
    public void launchApplication(String URL){
        driver.get(URL);
    }
}
