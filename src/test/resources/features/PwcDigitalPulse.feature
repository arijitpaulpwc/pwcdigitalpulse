Feature: PwC Digital Pulse
  @Sanity
  Scenario Outline: Verify the number of column is displayed at HomePage
    Given I navigate to the PwC Digital Pulse website
    When I am viewing the "<PageName>" page
    Then I am presented with "<ColumnNumber>" columns of articles
    And The ‘left’ column is displaying "<LeftColumnArticle>" articles
    And The ‘middle’ column is displaying "<MiddleColumnArticle>" articles
    And The ‘right’ column is displaying "<RightColumnArticle>" articles
    Examples:
    |PageName|ColumnNumber|LeftColumnArticle|MiddleColumnArticle|RightColumnArticle|
    |  Home  |    3       |       2         |         1         |         4        |

  @Regression
  Scenario Outline: Verify Contact Us Form
    Given I navigate to the PwC Digital Pulse website
    And I am viewing the "<PageName>" page
    When I click on the 'Subscribe' navigation link
    Then I am taken to the 'Subscribe' page
    And I am presented with the below fields
      | Field                 | Required  | Type      |
      | First Name            |true       |text       |
      | Last Name             |true       | text      |
      |Organisation           |true       | text      |
      |Job Title              |true       | text      |
      |Business Email Address | true      | text      |
      |State                  | true      | dropdown  |
      |Countries or regions   |true       | dropdown  |
    And I will need to complete Google reCAPTCHA before I can complete my request

    Examples:
      |PageName |
      |Home     |

  @Sanity
  Scenario Outline: Verify the Search Functionality
    Given I navigate to the PwC Digital Pulse website
    When I click on the ‘Magnifying glass’ icon to perform a search
    And I enter the text "<SearchText>"
    And I submit the search
    Then I am taken to the search results page
    And I am presented with at least "<MinNoOfSearchResult>" search result
    Examples:
    |SearchText              | MinNoOfSearchResult |
    |Single page applications|    1                |