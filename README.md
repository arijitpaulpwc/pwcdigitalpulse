This is for validating the PwC Digital Pulse website.

Pre Requisite :- 
1. JDK
2. Maven
3. IntelliJ or Eclipse IDE
4. Cucumber plugin should be installed in the project
5. TestNG plugin should be installed in the project

Steps to Execute the Test:-
1. Take a Clone of the project. If you have the Project repo in your system please take a pull before starting.
2. Open the Project Repo in the IDE.
3. Build the project to download all the Maven Dependecies.
4. Execute the Cucumber Runner file to execute the feature files. Runner File Location - src/test/java/runner/CucumberRunner
5. Senarios are tagged with @Sanity and @Regression in the feature file.
6. Same tags(@Sanity and @Regression) are used in the runner file to run the scenarios.
7. Check the report folder for the cucumber-report.html
8. Check the report/spark folder for the spark.html report

** Last Execution reports are present in the report folder.
** Spark Reports are present in the report folder. For the Failed Scenario, Screenshot will be attach in the report which will helps the tester for further investigation and defect logging.
** Regarding chromeDriver if any exception occur due to the Google Chrome version. Please download the relevent chromeDriver version and place the .exe file into the /driver folder and Run the Runner file.

